difference()
{
    union()
    {
        translate([20.5,-12.5,0])
            import("../original/XY_Joiner_8mm_One_Piece_Make_2.stl");

        // plug up original positions:

        // toothed idler

        translate([2.75,-6,21])
        rotate([15,0,0])
        rotate([-90,0,90])
            cylinder(d=6.5, $fn=6, h=19.5);

        translate([2.75,-6,21])
        rotate([15,0,0])
        rotate([-90,0,90])
        {
            translate([0-1.04,0-1.04,14.5])
            cylinder(d=7, $fn=45, h=5);
        }

        // smooth idler

        translate([-2.75,6,34])
        rotate([-30,0,0])
        rotate([90,0,90])
            cylinder(d=6.5, $fn=6, h=19.5);

        translate([-2.75,6,34])
        rotate([90,0,90])
        {
            translate([0+1.04,0+1.04,14.5])
                cylinder(d=9, $fn=90, h=5);

            translate([0+1.04,0+1.04,0.5])
                cylinder(d=9, $fn=90, h=5);
        }

    }
        
    // move 1.08 mm on diagonal (1.04 mm on Y & Z)

    // toothed idler

    translate([2.75,-6-1.04,21+1.04])
    rotate([30,0,0])
    rotate([-90,0,90])
    {
        translate([0,0,-3])
            cylinder(d=6.5, $fn=6, h=6); // nut trap

            cylinder(d=3.5, $fn=30, h=20); // through hole
    }

    adj=.5;

    translate([-2.6+adj/2,-6-1.04,21+1.04])
    rotate([-90,0,90])
        cylinder(d=19, $fn=180, h=9.25+adj); // space for idler

    translate([-2.6-9.25-adj/2,-6-30-1.04,21-19/2+1.04])
        cube([9.25+adj,30,19]); // space to insert idler

    // smooth idler

    translate([-2.75,6+1.04,34+1.04])
    rotate([-30,0,0])
    rotate([90,0,90])
    {
        cylinder(d=6.5, $fn=6, h=3); // nut trap
        cylinder(d=3.5, $fn=30, h=20); // through hole
    }

    translate([2.6-adj/2,6+1.04,34+1.04])
    rotate([90,0,90])
        cylinder(d=19, $fn=180, h=9.25+adj); // space for idler

    // bore out too-small holes for X rods

    translate([25.75,0,-50])
        cylinder(d=10.5, $fn=45, h=100);
    translate([-25.75,0,-50])
        cylinder(d=10.5, $fn=45, h=100);

    // bore out too-small hole for Y bearing

    translate([0,30,0])
    rotate([90,0,0])
        cylinder(d=15.5, $fn=90, h=60);

    // notch the top of the bore to catch bridging

    translate([-1.5,-30,7.5])
        cube([3,60,1]);

}

    