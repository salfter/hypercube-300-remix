$fn=90;

difference()
{
    union()
    {
        // import original

        translate([20.5,-12.5,0])
            import("../original/XY_Joiner_8mm_One_Piece_Make_2.stl");

        // fill gaps

        translate([0,-14,-10.42])
            cube([3,12,4], center=true);

        translate([0,14,-10.42])
            cube([3,12,4], center=true);

        // fill screw holes that we no longer need

        translate([0,-14,-9.42])
            cube([25,7,6], center=true);

        translate([0,14,-9.42])
            cube([25,7,6], center=true);

        // spiral bushing bore

        rotate([90,0,0])
        minkowski()
        {
            cylinder(d=18, h=48, center=true);
            sphere(d=1);
        }

        // support rib for bushing bore

        translate([0,24,-12.4])
        rotate([90,0,0])
        linear_extrude(48, convexity=true)
            polygon([[-2,0],[-4,4],[4,4],[2,0]]);
    }

    // hollow out bushing bore

    rotate([90,0,0])
        cylinder(d=15, h=49, center=true);

    // add bushing retainer pin holes

    translate([0,-23,6])
    rotate([0,90,0])
        cylinder(d=0.9, h=35, center=true);

    translate([0,23,6])
    rotate([0,90,0])
        cylinder(d=0.9, h=35, center=true);

    // trim flat at screw holes

    translate([-12,-14,-11.3])
    rotate([0,45,0])
        cube([12,7,7], center=true); 

    translate([12,-14,-11.3])
    rotate([0,-45,0])
        cube([12,7,7], center=true); 

    translate([-12,14,-11.3])
    rotate([0,45,0])
        cube([12,7,7], center=true); 

    translate([12,14,-11.3])
    rotate([0,-45,0])
        cube([12,7,7], center=true); 
}

