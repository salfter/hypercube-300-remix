rotate([0,0,90])
translate([5.15,-3.375,43.28])
    %import("E3D_v6_To_Spec.stl");

translate([0,0,19])
rotate([90,0,90])
color([0,0,1,.5])
    %import("V6.6_Duct.stl");

translate([0,-15,34])
rotate([0,0,-90])
    %import("Fan 30mm.stl");

translate([0,33,55.5])
rotate([90,0,0])
    import("HC_E3D_mount_Make_1_.stl");

translate([0,-11,55.5])
rotate([-90,0,0])
    import("HC_E3D_clamp_Make_1.stl");

translate([0,26,59])
rotate([180,0,0])
    import("HC_Fan_Duct_Make_1.stl");

translate([28,10,27])
    import("HC_BLTouch_mount_Make_1.stl");

translate([25,0,45])
rotate([0,0,90])
    %import("BLTouch_Model.stl");

translate([-10,26,60])
rotate([90,0,0])
    %import("50mm_Fan.stl");

translate([15+25,35,0]) // BLTouch is 25 mm to right of nozzle
color([0,1,0,.5])
    %import("bltouch_83mm_calibration_spacer.stl");

translate([0,33,55])
rotate([-90,180,0])
    import("HC_X_Carrier_Make_1.stl");

translate([25,47.5,55])
rotate([-90,0,0])
    import("HC_X_Carriage_beltclamp_Make_1.stl");

translate([20,47,54.5])
rotate([0,90,180])
    import("HC_X_Carrier_Bushing_-No_IGUS_Make_2_tuned-10.6.stl");

translate([-20,47,54.5])
rotate([0,-90,180])
    import("HC_X_Carrier_Bushing_-No_IGUS_Make_2_tuned-10.6.stl");
