$fn=180;

// adjustments
id=10.8; // start with measured carbon-fiber rod OD
mul=1.16; // >=1.176: ridges, <1.176: flats

translate([25.21,0,0])
difference()
{
    union()
    {
        cylinder(d=16, h=40);

        difference()
        {
            translate([0,-1,0])
            minkowski()
            {
                rotate([90,0,0])
                linear_extrude(2.5, convexity=10)
                polygon([
                    [0,2],
                    [14,2],
                    [14,7],
                    [3,20],
                    [14,33],
                    [14,38],
                    [0,38],
                    [-14,22],
                    [-14,18],
                    [0,2]
                ]);

                sphere(2);
            }

            translate([11.25,5,4.75])
            rotate([90,0,0])
                cylinder(d=3.5, h=12);

            translate([11.25,5,35.25])
            rotate([90,0,0])
                cylinder(d=3.5, h=12);

            translate([-11.25,5,20])
            rotate([90,0,0])
                cylinder(d=3.5, h=12);


            translate([11.25,-2.5,4.75])
            rotate([90,30,0])
                cylinder(d=6.8, h=3, $fn=6);

            translate([11.25,-2.5,35.25])
            rotate([90,30,0])
                cylinder(d=6.8, h=3, $fn=6);

            translate([-11.25,-2.5,20])
            rotate([90,30,0])
                cylinder(d=6.8, h=3, $fn=6);
        }
    }

    linear_extrude(40, convexity=10, twist=36, slices=400)
    union()
    {
        circle(d=id*mul, $fn=5);
        rotate([0,0,36])
            circle(d=id*mul, $fn=5);
    }

    cylinder(d=id, h=40); 

    cylinder(d1=id+.5, d2=id, h=2);

    translate([0,0,39])
        cylinder(d2=id+.5, d1=id, h=2);
}


