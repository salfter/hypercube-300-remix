//translate([100,0,0])
union()
{
    rotate([90,0,180])
        import("HC_X_Carrier_Make_1.stl");

    translate([25,15,0])
    rotate([-90,0,0])
        import("HC_X_Carriage_beltclamp_Make_1.stl");

    translate([20,14,-.5])
    rotate([0,90,180])
        import("HC_X_Carrier_Spiral_Bushing_ID=10.6_Make_2.stl");

    translate([-20,14,.5])
    rotate([0,-90,180])
        import("HC_X_Carrier_Spiral_Bushing_ID=10.6_Make_2.stl");
}

union()
{
    rotate([90,0,0])
        import("HC_E3D_mount_Make_1_.stl");

    translate([0,-45,0])
    rotate([-90,180,0])
    import("HC_E3D_clamp_Make_1.stl");

    translate([28,-23,-28])
    {
        //include <HC_BLTouch_mount_Make_1.scad>;
        import("HC_BLTouch_mount_Make_1.stl");
    }

    scale([1,1,.9657]) // this scrapes the bed when level...make it 2 mm shorter
    translate([0,-7,3])
    rotate([0,180,180])
        import("HC_Fan_Duct_Make_1.stl");

    translate([-10,-7,5])
    rotate([90,0,0])
        %import("50mm_Fan.stl");

    translate([5.2,-36.5,-12])
        %import("E3D_v6_To_Spec.stl");
}

translate([25,0,0]) // probe offset from nozzle
translate([0,-33.15,-18.5+8.3])
rotate([0,0,-90])
    %import("BLTouch_Model.stl");

translate([0,0,-55.25])
translate([-50,-50,-1])
//color([1,1,1])
    %cube([100,100,1]);

translate([-25,17.9,42])
rotate([90,0,0])
    %import("ExtruderPlugPanel.stl");

translate([-20,19.7,32])
    //include <Panel_Mount.scad>;
    import("Panel_Mount.stl");