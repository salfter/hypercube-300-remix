difference()
{
    translate([23,-14,-30])
    {
        cube([5,14,15]);
        cube([5,7,31]);
    }

    translate([20,-3.5,-25.5])
    rotate([0,90,0])
    {
        cylinder(d=3.5, h=10, $fn=20);
        translate([0,0,6])
            cylinder(d=7, h=2, $fn=90);
    }

    translate([20,-3.5,-18])
    rotate([0,90,0])
    {
        cylinder(d=3.5, h=10, $fn=20);
        translate([0,0,6])
            cylinder(d=7, h=2, $fn=90);
    }
}

difference()
{
    translate([23,-46,-6])
        cube([8.4,39,8]);

    translate([22,-42.5,-2])
    rotate([0,90,0])
        cylinder(d=3, h=10, $fn=20);

    translate([22,-23.5,-2])
    rotate([0,90,0])
        cylinder(d=3, h=10, $fn=20);
}

// mockup below here

//translate([100,0,0])
union()
{
    rotate([90,0,180])
        %import("HC_X_Carrier_Make_1.stl");

    translate([25,15,0])
    rotate([-90,0,0])
        %import("HC_X_Carriage_beltclamp_Make_1.stl");

    translate([20,14,-.5])
    rotate([0,90,180])
        %import("HC_X_Carrier_Spiral_Bushing_ID=10.6_Make_2.stl");

    translate([-20,14,.5])
    rotate([0,-90,180])
        %import("HC_X_Carrier_Spiral_Bushing_ID=10.6_Make_2.stl");
}

union()
{
    rotate([90,0,0])
        %import("HC_E3D_mount_Make_1_.stl");

    translate([0,-45,0])
    rotate([-90,180,0])
        %import("HC_E3D_clamp_Make_1.stl");

    scale([1,1,.9657])
    translate([0,-7,3])
    rotate([0,180,180])
        %import("HC_Fan_Duct_Make_1.stl");

    translate([-10,-7,5])
    rotate([90,0,0])
        %import("50mm_Fan.stl");

    translate([5.2,-36.5,-12])
        %import("E3D_v6_To_Spec.stl");
}

translate([25,0,0]) // probe offset from nozzle
{
    translate([0,-33.15,-55.25])
    color([1,0,0])
        %cylinder(d=.5, h=100); // bottom is where the probe hits

    translate([0,17,-55.25+8.3]) // use same height as BLTouch
    rotate([0,0,180])
    {
        %import("../../electronics/bed level sensor/Body-V2.stl");
        translate([-5.5,37.75,28.75])
        rotate([180,0,90])
            %import("../../electronics/optical runout sensor/Optical endstop.stl");
        translate([-.25,50,-1.85])
            %import("../../electronics/bed level sensor/Flag-V2.stl");
        translate([0,50,1.5-9])
        rotate([-90,0,0])
            %import("Screw_Machine_M3x25.stl");
    }
}

translate([0,0,-55.25])
translate([-50,-50,-1])
//color([1,1,1])
    %cube([100,100,1]);

translate([-25,17.9,42])
rotate([90,0,0])
    %import("ExtruderPlugPanel.stl");

translate([-20,19.7,32])
    //include <Panel_Mount.scad>;
    %import("Panel_Mount.stl");

