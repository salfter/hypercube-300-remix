//Belt Clamp for Mostly Printed CNC Router
//Modification of design http://www.thingiverse.com/thing:627214
Ht6=6.51;
Thk=2.0;        //Base Thickness, I used 3mm initially
                //Test print shim showed 2mm comes out to 2.3mm which
                //fit snugly between wooden X-Carriage & Alum. extrusion
module DrawBase()
{
    color("red")
    translate([0,0,0])
    linear_extrude(height = Thk, center = false, convexity = 10)polygon(points = 
    [[8.13,3.75],[8.6,3.65],[9.01,3.38],
    [9.28,2.98],[9.38,2.5],[9.38,-3.25],
    [9.28,-3.87],[8.99,-4.43],[8.55,-4.87],
    [7.99,-5.15],[7.38,-5.25],[-2.97,-5.25],
    [-3.19,-5.22],[-8.36,-3.62],[-8.89,-3.27],
    [-9.25,-2.73],[-9.38,-2.11],[-9.38,2.5],
    [-9.28,2.98],[-9.01,3.38],[-8.6,3.65],
    [-8.13,3.75]]);
}
module ClampTop()
{
    color("cyan")
    translate([0,0,Thk-.01])
    linear_extrude(height = Ht6, center = false, convexity = 10)polygon(points = 
    [[-8.13,3.75],[-8.6,3.65],[-9.01,3.38],
    [-9.28,2.98],[-9.38,2.5],[-9.28,2.02],
    [-9.01,1.62],[-8.6,1.35],[-8.13,1.25],
    [8.13,1.25],[8.6,1.35],[9.01,1.62],
    [9.28,2.02],[9.38,2.5],[9.28,2.98],
    [9.01,3.38],[8.6,3.65],[8.13,3.75]]);
}
module ClampBot()
{
    color("cyan")
    translate([0,0,Thk-.01])
    linear_extrude(height = Ht6, center = false, convexity = 10)polygon(points = 
    [[-2.97,-5.25],[-3.19,-5.22],[-3.56,-4.96],
    [-3.72,-4.54],[-3.61,-4.11],[-3.27,-3.81],
    [0.66,-2.09],[1.29,-1.84],[1.95,-1.63],
    [2.62,-1.46],[3.3,-1.34],[3.98,-1.27],
    [4.67,-1.25],[7.38,-1.25],[7.99,-1.35],
    [8.55,-1.63],[8.99,-2.07],[9.28,-2.63],
    [9.38,-3.25],[9.28,-3.87],[8.99,-4.43],
    [8.55,-4.87],[7.99,-5.15],[7.38,-5.25]]);
}
module ClampMid()
{
    translate([0,0,Thk-.01])
    linear_extrude(height = Ht6, center = false, convexity = 10)polygon(points = 
    [[-7.07,-3.61],[-3.16,-1.9],[-2.79,-1.55],
    [-2.73,-1.06],[-2.99,-0.63],[-3.46,-0.46],
    [-7.73,-0.46],[-8.36,-0.59],[-8.89,-0.94],
    [-9.25,-1.48],[-9.38,-2.11],[-9.25,-2.73],
    [-8.89,-3.27],[-8.36,-3.62],[-7.73,-3.75]]);
}
module DrawUnion()
{
    union()
    {
        DrawBase();
        ClampBot();
        ClampMid();
        ClampTop();
    }
}
DrawUnion();
//Draw a mirrored copy also,
//so we can have end of belt on outside & going down on any side
translate([0,7.5+4,0])
mirror([0,1,0])
DrawUnion();