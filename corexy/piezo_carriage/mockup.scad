$fn=90;

translate([25,32,100])
rotate([90,0,-90])
color([1,0,0])
    import("Hypercube_300_Orion_Piezo_Carriage_Igus.stl");

translate([7,-17.5,60])
rotate([0,180,0])
color([0,1,0])
    import("Orion_Groove_Clamp_3 (no support).stl");

translate([10.5,-12,39.5])
rotate([0,0,90])
color([.9,.9,.9])
    import("E3D_v6_To_Spec.stl");

translate([42,41,68.5])
rotate([0,-90,90])
color([0,1,0])
    import("Hypercube_300_Piezo_Carriage_Fan_Duct.stl");

translate([20.3,18.2,55.3])
rotate([90,0,0])
color([0,1,0])
    import("HC300_Lite_X_Carriage_Belt_Clamp.stl");

translate([17.5,25,20])
rotate([90,0,180])
color([.1,.1,.1])
    import("50mm_Fan.stl");