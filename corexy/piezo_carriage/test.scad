$fn=30;

difference()
{
    union()
    {
        import("Hypercube_300_Piezo_Carriage.repaired.stl");

        translate([18.55,-27.55,0.5])
        minkowski()
        {
            cylinder(d=15.4, h=35.1);
            sphere(d=1);
        }

        translate([18.55,-79.05,0.5])
        minkowski()
        {
            cylinder(d=15.4, h=35.1);
            sphere(d=1);
        }
    }

    translate([18.55,-27.55,0])
        cylinder(d=12.4, h=36.1);

    translate([18.55,-79.05,0])
        cylinder(d=12.4, h=36.1);

    translate([18.55,-27.55,18.05])
        cylinder(d=14, h=3.2);

    translate([18.55,-79.05,18.05])
        cylinder(d=14, h=3.2);
}

translate([13.8,-45,5])
rotate([0,90,0])
    %import("HC300_Lite_X_Carriage_Belt_Clamp.stl");

