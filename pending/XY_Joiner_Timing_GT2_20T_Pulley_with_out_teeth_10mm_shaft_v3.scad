difference()
{
    import("XY_Joiner_Timing_GT2_20T_Pulley_with_out_teeth_8mm_shaft_v3.stl");
    translate([0,25.75,-2.5])
        cylinder(d=10.2, h=17, $fn=60);
    translate([0,-25.75,-2.5])
        cylinder(d=10.2, h=17, $fn=60);
}
