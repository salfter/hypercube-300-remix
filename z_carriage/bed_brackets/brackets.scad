$fn=30;

//mockup();

//bed_bracket_bottom();
//bed_bracket_top();
bed_bracket_pwr();

module mockup()
{
    mockup_bed();
    mockup_bed_frame();

    translate([167.5,-127.5,10.5])
        mockup_bed_bracket();

    translate([-167.5,-127.5,10.5])
    mirror([1,0,0])
        mockup_bed_bracket();

    translate([-167.5,127.5,10.5])
    rotate([0,0,180])
        mockup_bed_bracket();
    
    translate([167.5,127.5,10.5])
    rotate([0,0,180])
    {
        mirror([1,0,0])
        rotate([180,0,-45])
            bed_bracket_bottom();
        rotate([0,0,90])
            bed_bracket_pwr();
    }
}

module mockup_bed()
{
    translate([0,0,22])
        // %cube([12*25.4,12*25.4,25.4/4+2], center=true); // nominal
        %cube([306.5,305,25.4/4+2], center=true); // actual
}

module mockup_bed_frame()
{
    // extrusions

    translate([410/2-37.5,0,0])
    rotate([0,0,90])
        %cube([235,20,20], center=true);
    translate([-410/2+37.5,0,0])
    rotate([0,0,90])
        %cube([235,20,20], center=true);
    translate([0,235/2+10,0])
        %cube([410,20,20], center=true);
    translate([0,-235/2-10,0])
        %cube([410,20,20], center=true);

    // bearing holders

    translate([-410/2,-235/2-20,0])
    translate([90,-20.19,40.4])
    rotate([0,-90,0])
        %import("../original/HC300_Z_Carriage_Left_Make_2.stl");
    mirror([0,1,0])
    translate([-410/2,-235/2-20,0])
    translate([90,-20.19,40.4])
    rotate([0,-90,0])
        %import("../original/HC300_Z_Carriage_Left_Make_2.stl");
    mirror([1,0,0])
    translate([-410/2,-235/2-20,0])
    translate([90,-20.19,40.4])
    rotate([0,-90,0])
        %import("../original/HC300_Z_Carriage_Left_Make_2.stl");
    mirror([1,0,0])
    mirror([0,1,0])
    translate([-410/2,-235/2-20,0])
    translate([90,-20.19,40.4])
    rotate([0,-90,0])
        %import("../original/HC300_Z_Carriage_Left_Make_2.stl");

    // brackets

    translate([-410/2+37.4,-235/2-10,0])
    rotate([90,0,90])
        %import("../../frame_braces/hc300_z_carriage_t_bracket.stl");
    mirror([1,0,0])
    translate([-410/2+37.4,-235/2-10,0])
    rotate([90,0,90])
        %import("../../frame_braces/hc300_z_carriage_t_bracket.stl");
    mirror([0,1,0])
    translate([-410/2+37.4,-235/2-10,0])
    rotate([90,0,90])
        %import("../../frame_braces/hc300_z_carriage_t_bracket.stl");
    mirror([1,0,0])
    mirror([0,1,0])
    translate([-410/2+37.4,-235/2-10,0])
    rotate([90,0,90])
        %import("../../frame_braces/hc300_z_carriage_t_bracket.stl");
}

module bed_bracket_bottom()
{
    rotate([180,0,-45])
    {
        difference()
        {
            translate([-105,-50,0])
            rotate([0,0,45])
                cube([200,20,2]);

            translate([10.5,56,-0.5])
            cube([50,50,3]);

            translate([-120,-75.5,-0.5])
            cube([50,50,3]);

            translate([0,69,-0.5])
                cylinder(d=5.4, h=3);

            translate([-69,0,-0.5])
                cylinder(d=5.4, h=3);
        }

        translate([10.1,65,-20])
            cube([2,29,22]);

        translate([-12.1,45.1,-20])
            cube([2,26,22]);

        translate([-93,-12.1,-20])
            cube([26,2,22]);

        translate([-71,10.1,-20])
            cube([26,2,22]);
    }
}

module bed_bracket_top($skip)
{
    rotate([0,0,45])
    translate([0,0,2.5])
    {
        difference()
        {
            translate([-105,-50,0])
            rotate([0,0,45])
                cube([200,20,4]);

            translate([10.5,56,-0.5])
            cube([50,50,5]);

            translate([-120,-75.5,-0.5])
            cube([50,50,5]);

            translate([0,69,-0.5])
            {
                cylinder(d=5.4, h=5);
                translate([0,0,1.5])
                    cylinder(d=12, h=3);
            }


            translate([-69,0,-0.5])
            {
                cylinder(d=5.4, h=5);
                translate([0,0,1.5])
                    cylinder(d=12, h=3);
            }
        }

        translate([-14.1,44.5,0])
            cube([3.5,20,15]);

        if ($skip!="y")
        translate([-109,-29.1,0])
            cube([28,4,15]);
    }
}

module bed_bracket_pwr() // 55-105 mm from right edge needs to be kept clear for heater wiring
{
    difference()
    {
        union()
        {
            rotate([0,0,-45])
            mirror([1,0,0])
                bed_bracket_top($skip="y");
            translate([0,0,2.5])
            {
                translate([-75,-124,0])
                    %cube([50,54,4]);

                translate([-29.1,-144,0])
                    cube([20,94,4]);

                translate([-29.1,-144,0])
                    cube([4,18,15]);

                translate([-29.1,-68,0])
                    cube([4,18,15]);
            }
        }

        translate([0,-69,3.5]) // need to re-countersink this hole
            cylinder(d=12, h=10);
    }
}

module mockup_bed_bracket()
{
    rotate([0,0,-45])
        bed_bracket_top();
    rotate([180,0,-45])
        bed_bracket_bottom();
}
