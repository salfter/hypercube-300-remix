$fn=30;

difference()
{
    union()
    {
        translate([-24,-5,-18.28])
        difference()
        {
            rotate([0,0,180])
            translate([23.5,-16,0])
                import("AM8_Right_Side_Y_Chain_Link.stl");

            translate([0,-22,0])
                cube([30,20,20]);
        }

        translate([-24,-10,-18])
        {
            cube([5,3,20]);
            cube([18,3,3]);
        }

        translate([-6.5,-10,-18])
            cube([5,3,20]);

        difference()
        {
            translate([-24,-27,-3])
                cube([24,20,2]);
    
            translate([-12,-17,-5])
                cylinder(d=5.6, h=10);
        }
    }

    translate([-24,-27,-1])
        cube([24,20,3]);
}

translate([-3.5,-10,-3])
rotate([90,180,90])
linear_extrude(2, convexity=10)
    polygon([[0,0],[0,10],[10,0]]);

translate([-24,-10,-3])
rotate([90,180,90])
linear_extrude(2, convexity=10)
    polygon([[0,0],[0,10],[10,0]]);
