nom_width=6;
tolerance=0.3;

trim=1;

difference()
{
	translate([17,51,-7])
		import("Pillow_Block_Bearing_Spacer.stl");

	translate([-40,(nom_width-tolerance)/2,0])
		cube([80,1,10]);

	translate([-40,-(nom_width-tolerance)/2-1,0])
		cube([80,1,10]);

	translate([0,0,-7+trim/2])
		cube([80,25,trim], center=true);
}

