translate([0,-25,0])
{
rotate([90,180,0])
    import("tmp/PK000.stl"); // pillow block bearing

translate([0,0,22])
union()
{
    translate([-70,25,0])
    rotate([0,180,0])
        import("stepper_mount_mod.stl"); // motor mount (center at z=22)

    translate([-105,0,40])
    rotate([0,180,0])
    union()
    {
        import("tmp/Nema 17HS4401-S.stl"); // motor
         translate([0,0,61])
         rotate([0,90,0])
             import("tmp/GT2_20T.STL"); // motor pulley
        translate([-50,0,50])
            cube([200,1,6],center=true); // belt alignment test object
    }
}

translate([0,0,23])
rotate([0,90,0])
    import("tmp/GT2_20T-sh8.stl"); // leadscrew pulley
}

translate([-200,0,10])
rotate([0,90,0])
scale([1,1,2])
import("tmp/100mm_Beam.stl");