$fn=45;

use <leveler/leveler.scad>

//model(); // view frame within bed assembly
production(); // view frame halves by themselves

module production()
{
    // translate([0,350,0])
    //     bed_frame_top();
    // bed_frame_bottom();
    bed_frame_norm();
}
    
module bed_frame_bottom()
{
    difference()
    {
        bed_frame_norm();
        translate([0,0,2.5])
            cube([270,315,11]);
    }
}

module bed_frame_top()
{
    translate([0,0,-2.5])
    difference()
    {
        bed_frame_norm();
            cube([270,315,2.5]);
    }
}

module bed_frame_norm()
{
    translate([-70,20,-25])
        bed_frame();
}

module extrusion(h)
{
    color("gray")
    translate([-25,30,0])
    linear_extrude(h, convexity=10)
        import("20mm-tslot/20mm-M5-W1pt6-X1.dxf");
}

module z_carriage()
{
    // extrusions

    translate([0,0,20])
    {
        translate([25,20,0])
        rotate([-90,0,0])
            %extrusion(235);
        translate([365,20,0])
        rotate([-90,0,0])
            %extrusion(235);
        rotate([0,90,0])
            %extrusion(410);
        translate([0,255,0])
        rotate([0,90,0])
            %extrusion(410);
    }

    // brackets

    translate([35,10,10])
    rotate([90,0,90])
    color("red")
        %import("../frame_braces/hc300_z_carriage_t_bracket.stl");

    translate([375,10,10])
    rotate([90,0,90])
    color("red")
        %import("../frame_braces/hc300_z_carriage_t_bracket.stl");

    translate([35,265,10])
    rotate([90,0,-90])
    color("red")
        %import("../frame_braces/hc300_z_carriage_t_bracket.stl");

    translate([375,265,10])
    rotate([90,0,-90])
    color("red")
        %import("../frame_braces/hc300_z_carriage_t_bracket.stl");

    translate([90,-20,50])
    rotate([0,-90,0])
    color("red")
        %import("../z_carriage/original/HC300_Z_Carriage_Left_Make_2.stl");

    translate([320,-20,50])
    mirror([1,0,0])
    rotate([0,-90,0])
    color("red")
        %import("../z_carriage/original/HC300_Z_Carriage_Left_Make_2.stl");

    translate([320,295,50])
    rotate([0,-90,180])
    color("red")
        %import("../z_carriage/original/HC300_Z_Carriage_Left_Make_2.stl");

    translate([90,295,50])
    mirror([1,0,0])
    rotate([0,-90,180])
    color("red")
        %import("../z_carriage/original/HC300_Z_Carriage_Left_Make_2.stl");

    // levelers

    color("red")
    translate([70,0,20])
    rotate([0,180,90])
        %frame_mount();

    color("red")
    translate([320,0,20])
    rotate([0,180,90])
        %frame_mount();

    color("red")
    translate([215,275,20])
    rotate([0,180,-90])
        %frame_mount();
}

module screw_locations()
{
    // screw locations

    translate([80,-10,15])
    {
        %cylinder(d=1, h=10);
        translate([0,0,12])
        rotate([180,0,0])
        color("red")
            %screw_foot();
    }

    translate([330,-10,15])
    {
        %cylinder(d=1, h=10);
        translate([0,0,12])
        rotate([180,0,0])
        color("red")
            %screw_foot();
    }

    translate([205,285,15])
    {
        %cylinder(d=1, h=10);
        translate([0,0,12])
        rotate([180,0,0])
        color("red")
            %screw_foot();
    }
}

module bed()
{
    translate([55,-12.5,30])
    color("silver")
        %cube([300,300,6.35]);
}

module model()
{
    z_carriage();
    screw_locations();
    bed();
    bed_frame();
}

module bed_frame()
{
    translate([70,-20,25])
        front_edge();
    translate([320,-20,25])
        front_edge();
    translate([215,295,25])
    rotate([0,0,180])
        front_edge();

    translate([70,20,25])
    rotate([0,0,-28])
        cube([20,273,5]);

    translate([340,20,25])
    mirror([-1,0,0])
    rotate([0,0,-28])
        cube([20,273,5]);

    translate([80,10,25])
        cube([250,20,5]);
}

module front_edge()
{
    difference()
    {
        cube([20,40,5]);
        translate([10,10,0])
            cylinder(d=10.5, h=2);
    }
    translate([0,0,2])
    cube([20,7,8]);
}

