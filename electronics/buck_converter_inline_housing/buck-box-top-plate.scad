// buck-converter housing: top-plate
// by Nicholas J. Fiorello Jr.
// Designed for:
//  "eBoot 6 Pack LM2596 DC to DC Buck Converter
//  3.0-40V to 1.5-35V Power Supply Step Down Module"
//  https://www.amazon.com/gp/product/B01GJ0SC2C
include <buck-box-variables.scad>;

module topPlate() {
  $fn=36;
  plateHeight = 10;

  module mount() {
    difference() {
      union() {
        cylinder(d=5.0, h=14 - buckThick);
        // flat to attach to side
        translate([-2.5,-2.5,0]) cube([5,2.5,plateHeight - boxThick]);
      }
      // m3 threaded hole 2.5mm?
      translate([0, 0, -1]) cylinder(d=2.8, h=14);
    }
  }

  // mounting pillars
  translate([0,0,boxThick]) {
    translate([boxX + mountX, boxY + mountY, 0]) mount();
    translate([boxLength - (boxX + mountX), boxWidth - (boxY + mountY), 0]) {
      rotate([0,0,180]) mount();
    }
  }

  // box
  difference() {
    cube([boxLength, boxWidth, plateHeight]);
    // carve out box interior
    translate([boxThick, boxThick, boxThick]) {
      cube([boxLength - (boxThick * 2), boxWidth - (boxThick  * 2), plateHeight]);
    }
    // carve out hole to adjust trimmer
    translate([trimX, trimY ,-1]) cylinder(d=trimmerHole, h=3);
    // carve hole for wires.
    translate([boxLength / 2, boxWidth / 2, plateHeight]) {
      rotate([0,90,0]) {
        cylinder(d=wireHole, h=boxLength + 2, center=true);
      }
    }

    // slice off end box for alignment of pillars
    // translate([-1,-1,-1]) cube([boxX + mountX, boxWidth + 2, 25]);
  }
}

difference()
{
  topPlate();

  translate([10, 10, 0])
    cylinder(d=3.5, h=5, $fn=20);
  translate([boxLength-10, 10, 0])
    cylinder(d=3.5, h=5, $fn=20);
}
