// buck-converter housing: bottom-plate
// by Nicholas J. Fiorello Jr.
// Designed for:
//  "eBoot 6 Pack LM2596 DC to DC Buck Converter
//  3.0-40V to 1.5-35V Power Supply Step Down Module"
//  https://www.amazon.com/gp/product/B01GJ0SC2C
include <buck-box-variables.scad>

module bottomPlate() {
  $fn=36;
  plateHeight = 10;
  module mount() {
    difference() {
      cylinder(d=6, h=3);
      // m3 threaded hole
      translate([0, 0, -1]) cylinder(d=3.2, h=5);
    }
  }

  // mounting holes
  px = boxX + mountX;
  py = boxY + mountY;

  translate([px, boxWidth - py, boxThick]) mount();
  translate([boxLength - px, py, boxThick]) mount();

  // box
  difference() {
    cube([boxLength, boxWidth, plateHeight]);
    // carve out the box interior
    translate([boxThick, boxThick, boxThick]) {
      cube([boxLength - (boxThick * 2), boxWidth - (boxThick  * 2), plateHeight]);
    }
    // carve out hole for wires
    translate([boxLength / 2, boxWidth / 2, plateHeight]) {
      rotate([0,90,0]) {
        cylinder(d=wireHole, h=boxLength + 2, center=true);
      }
    }

    // indent for screws
    translate([px, boxWidth - py, -.1]) cylinder(d1=4.5, d2=3, h=2);
    translate([boxLength - px, py, -.1]) cylinder(d1=4.5, d2=3, h=2);

    // slice box for alignment of pillars
    // translate([-1,-1,-1]) cube([px, boxWidth + 2, 25]);

  }
}

bottomPlate();