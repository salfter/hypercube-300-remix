// this is just all the other scad files in the project concatenated into one
//  so (hopefully) thingiverse can display it
// buck-converter housing
// by Nicholas J. Fiorello Jr.
// Designed for:
//  "eBoot 6 Pack LM2596 DC to DC Buck Converter
//  3.0-40V to 1.5-35V Power Supply Step Down Module"
//  https://www.amazon.com/gp/product/B01GJ0SC2C
buckLength=43.25;
buckWidth=21.25;
buckHeight = 13;
buckThick=1.4;
mountX=6.5;
mountY=2.5;
boxLength = buckLength + 14;
boxWidth = buckWidth + 3;
boxThick = 1.5;
boxX = (boxLength - buckLength) / 2;
boxY = (boxWidth - buckWidth) / 2;
trimX = boxX + 10.6 + 9.4 - (2.1/2);
trimY = boxWidth - boxY - 1.9 - (2.1/2);
wireHole = 3.5;
trimmerHole = 2.7;
module converter() {
  $fn=72;
  module board() {
    difference() {
      union() {
        color("green") cube([buckLength, buckWidth, buckThick]);
        color("silver")  {
          translate([.1,.1, -.1]) cube([3, 3, 1.6]);
          translate([buckLength -3.1, .1, -.1]) cube([3, 3, 1.6]);
          translate([buckLength -3.1, buckWidth - 3.1, -.1]) cube([3, 3, 1.6]);
          translate([.1, buckWidth - 3.1, -.1]) cube([3, 3, 1.6]);
          translate([buckLength - 6.5, 2.5, -.1]) cylinder(d=4, h=1.6);
          translate([6.5, buckWidth - 2.5, -.1]) cylinder(d=4, h=1.6);
        }
      }
      color("green") {
        translate([buckLength - mountX, mountY,0]) cylinder(d=3, h=4, center=true);
        translate([mountX, buckWidth - mountY, 0]) cylinder(d=3, h=4, center=true);
        translate([1.6,1.6,0]) cylinder(d=1,h=4, h=10, center=true);
        translate([buckLength - 1.6, 1.6, 0]) cylinder(d=1,h=4, h=10, center=true);
        translate([buckLength - 1.6,buckWidth - 1.6,0]) cylinder(d=1,h=4, h=10, center=true);
        translate([1.6, buckWidth - 1.6,0]) cylinder(d=1,h=4, h=10, center=true);
      }
    }
  }
  module capacitor() {
    color("silver") cylinder(d=7.9, h=12.1 - buckThick);
  }
  module trimmer() {
    dx = 2.1 / 2;
    color("blue") cube([9.4, 4.2, 11.9 - buckThick]);
    color("gold") translate([9.4 - dx, dx, 13 - buckThick - 2]) {
      rotate([0,0,25]) {
         difference() {
          cylinder(d=2.1,h=2);
          translate([-1.5,-.25, 1.5]) cube([3, .5, 1]);
         }
      }
    }
  }
  module core() {
    color("black") cube([11.8, 11.8, 9 - buckThick]);
  }
  module regulator() {
    color("silver") cube([10.4, 2, 1]);
    color("black") translate([0,2,0]) cube([10.4, 8.3, 6.2 - buckThick]);
    color("silver") {
      for (i = [0:2:6] ) {
        translate([i + 1.75, 0, 0]) {
          translate([0, 9, 1]) cube([1, 4,.5]);
          translate([0, 13, 0]) cube([1, 2,.5]);
          translate([0, 12, 0]) cube([1, 1, 1.5]);
        }
      }
    }
  }
  module cap() {
    color("black") cube([4, 3, 3.8 - buckThick]);
  }
  union() {
    board();
    translate([0,0, buckThick]) {
      translate([4.5, buckWidth / 2, 0]) capacitor();
      translate([buckLength - 4.5, buckWidth / 2, 0]) capacitor();
      translate([10.6, 1.9, 0]) trimmer();
      translate([10.6, 8.3, 0]) core();
      translate([23.5, 1.25, 0]) regulator();
      translate([29, buckWidth -4.2, 0]) cap();
    }
  }
}
module bottomPlate() {
  $fn=36;
  plateHeight = 10;
  module mount() {
    difference() {
      cylinder(d=6, h=3);
      translate([0, 0, -1]) cylinder(d=3.2, h=5);
    }
  }
  px = boxX + mountX;
  py = boxY + mountY;
  translate([px, boxWidth - py, boxThick]) mount();
  translate([boxLength - px, py, boxThick]) mount();
  difference() {
    cube([boxLength, boxWidth, plateHeight]);
    translate([boxThick, boxThick, boxThick]) {
      cube([boxLength - (boxThick * 2), boxWidth - (boxThick  * 2), plateHeight]);
    }
    translate([boxLength / 2, boxWidth / 2, plateHeight]) {
      rotate([0,90,0]) {
        cylinder(d=wireHole, h=boxLength + 2, center=true);
      }
    }
    translate([px, boxWidth - py, -.1]) cylinder(d1=4.5, d2=3, h=2);
    translate([boxLength - px, py, -.1]) cylinder(d1=4.5, d2=3, h=2);
  }
}
module topPlate() {
  $fn=36;
  plateHeight = 10;
  module mount() {
    difference() {
      union() {
        cylinder(d=5.0, h=14 - buckThick);
        translate([-2.5,-2.5,0]) cube([5,2.5,plateHeight - boxThick]);
      }
      translate([0, 0, -1]) cylinder(d=2.8, h=14);
    }
  }
  translate([0,0,boxThick]) {
    translate([boxX + mountX, boxY + mountY, 0]) mount();
    translate([boxLength - (boxX + mountX), boxWidth - (boxY + mountY), 0]) {
      rotate([0,0,180]) mount();
    }
  }
  difference() {
    cube([boxLength, boxWidth, plateHeight]);
    translate([boxThick, boxThick, boxThick]) {
      cube([boxLength - (boxThick * 2), boxWidth - (boxThick  * 2), plateHeight]);
    }
    translate([trimX, trimY ,-1]) cylinder(d=trimmerHole, h=3);
    translate([boxLength / 2, boxWidth / 2, plateHeight]) {
      rotate([0,90,0]) {
        cylinder(d=wireHole, h=boxLength + 2, center=true);
      }
    }
  }
}
// put all the pieces together
translate([-boxLength/2,-boxWidth/2,0]) {
  translate([0, boxWidth, 30]) rotate([180,0,0]) topPlate();
  translate([boxX, boxY, 0]) converter();
  translate([0, 0, -20]) bottomPlate();
}
