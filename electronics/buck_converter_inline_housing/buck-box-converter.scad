// buck-converter housing: converter
// by Nicholas J. Fiorello Jr.
// Designed for:
//  "eBoot 6 Pack LM2596 DC to DC Buck Converter
//  3.0-40V to 1.5-35V Power Supply Step Down Module"
//  https://www.amazon.com/gp/product/B01GJ0SC2C
include <buck-box-variables.scad>

// A mockup model of the converter just because...
// ...ok so maybe I just got carried away
module converter() {
  $fn=72;
  module board() {
    difference() {
      union() {
        color("green") cube([buckLength, buckWidth, buckThick]);
        color("silver")  {
          // corner pads
          translate([.1,.1, -.1]) cube([3, 3, 1.6]);
          translate([buckLength -3.1, .1, -.1]) cube([3, 3, 1.6]);
          translate([buckLength -3.1, buckWidth - 3.1, -.1]) cube([3, 3, 1.6]);
          translate([.1, buckWidth - 3.1, -.1]) cube([3, 3, 1.6]);
          // hole rings
          translate([buckLength - 6.5, 2.5, -.1]) cylinder(d=4, h=1.6);
          translate([6.5, buckWidth - 2.5, -.1]) cylinder(d=4, h=1.6);
        }
      }
      color("green") {
        translate([buckLength - mountX, mountY,0]) cylinder(d=3, h=4, center=true);
        translate([mountX, buckWidth - mountY, 0]) cylinder(d=3, h=4, center=true);
        translate([1.6,1.6,0]) cylinder(d=1,h=4, h=10, center=true);
        translate([buckLength - 1.6, 1.6, 0]) cylinder(d=1,h=4, h=10, center=true);
        translate([buckLength - 1.6,buckWidth - 1.6,0]) cylinder(d=1,h=4, h=10, center=true);
        translate([1.6, buckWidth - 1.6,0]) cylinder(d=1,h=4, h=10, center=true);
      }
    }
  }
  module capacitor() {
    color("silver") cylinder(d=7.9, h=12.1 - buckThick);
  }
  module trimmer() {
    dx = 2.1 / 2;
    color("blue") cube([9.4, 4.2, 11.9 - buckThick]);
    color("gold") translate([9.4 - dx, dx, 13 - buckThick - 2]) {
      rotate([0,0,25]) {
         difference() {
          cylinder(d=2.1,h=2);
          translate([-1.5,-.25, 1.5]) cube([3, .5, 1]);
         }
      }
    }
  }
  module core() {
    color("black") cube([11.8, 11.8, 9 - buckThick]);
  }
  module regulator() {
    color("silver") cube([10.4, 2, 1]);
    color("black") translate([0,2,0]) cube([10.4, 8.3, 6.2 - buckThick]);
    color("silver") {
      for (i = [0:2:6] ) {
        translate([i + 1.75, 0, 0]) {
          translate([0, 9, 1]) cube([1, 4,.5]);
          translate([0, 13, 0]) cube([1, 2,.5]);
          translate([0, 12, 0]) cube([1, 1, 1.5]);
        }
      }
    }
  }
  module cap() {
    color("black") cube([4, 3, 3.8 - buckThick]);
  }
  union() {
    board();
    // evreything else sits on the board
    translate([0,0, buckThick]) {
      translate([4.5, buckWidth / 2, 0]) capacitor();
      translate([buckLength - 4.5, buckWidth / 2, 0]) capacitor();
      translate([10.6, 1.9, 0]) trimmer();
      translate([10.6, 8.3, 0]) core();
      translate([23.5, 1.25, 0]) regulator();
      translate([29, buckWidth -4.2, 0]) cap();
    }
  }
}

converter();