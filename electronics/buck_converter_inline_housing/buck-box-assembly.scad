// buck-converter housing: assembly
// by Nicholas J. Fiorello Jr.
// Designed for:
//  "eBoot 6 Pack LM2596 DC to DC Buck Converter
//  3.0-40V to 1.5-35V Power Supply Step Down Module"
//  https://www.amazon.com/gp/product/B01GJ0SC2C
include <buck-box-variables.scad>
use <buck-box-converter.scad>
use <buck-box-bottom-plate.scad>
use <buck-box-top-plate.scad>

// put all the pieces together
translate([-boxLength/2,-boxWidth/2,0]) {
  translate([0, boxWidth, 30]) rotate([180,0,0]) topPlate();
  translate([boxX, boxY, 0]) converter();
  translate([0, 0, -20]) bottomPlate();
}
