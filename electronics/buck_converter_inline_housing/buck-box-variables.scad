// buck-converter housing: variables
// by Nicholas J. Fiorello Jr.

// buck-converter measurements
//  L = 43.25
//  W = 21.25
//  H = 13.50
//  mounting holes 3mm diameter, 6.5mm from short side, 2.5mm from long side

// size of the unit
buckLength=43.25;
buckWidth=21.25;
buckHeight = 13;
buckThick=1.4;

// offsets to the mounting holes in the PCB
mountX=6.5;
mountY=2.5;

// Leave some space in the box for strain relief.
boxLength = buckLength + 14;
// .5 around the sides
boxWidth = buckWidth + 3;
boxThick = 1.5;

// Offsets to match box to board
boxX = (boxLength - buckLength) / 2;
boxY = (boxWidth - buckWidth) / 2;
trimX = boxX + 10.6 + 9.4 - (2.1/2);
trimY = boxWidth - boxY - 1.9 - (2.1/2);

// holes for running wires and accessing trimmer
wireHole = 3.5;
trimmerHole = 2.7;