$fn=30;

translate([-75.75,438.25,41])
rotate([0,0,-90])
%import("SKR-Pro-1.1-Complete-board.stl");

w=138;
h=86.5;
hd=3.1;

bw=10;
bt=3;

difference()
{
    union()
    {
        translate([-bw/2,-bw/2,0])
        {
            cube([w+bw,bw,bt]);
            cube([bw,h+bw,bt]);
        }
        translate([w-(bw/2),-bw/2,0])
            cube([bw,h+bw,bt]);
        translate([-bw/2,h-20+(bw/2),0])
            cube([w+bw,20,bt]);

        cylinder(d=hd+6, h=bt+4);
        translate([w,0,0])
            cylinder(d=hd+6, h=bt+4);
        translate([0,h,0])
            cylinder(d=hd+6, h=bt+4);
        translate([w,h,0])
            cylinder(d=hd+6, h=bt+4);

        translate([0,-(bw-2)/2,0])
        rotate([0,0,atan(h/w)])
            cube([sqrt(w*w+h*h),bw-2,bt]);

        translate([0,h-((bw-2)/2),0])
        rotate([0,0,-atan(h/w)])
            cube([sqrt(w*w+h*h),bw-2,bt]);
    }

    cylinder(d=hd, h=bt+4);
    translate([w,0,0])
        cylinder(d=hd, h=bt+4);
    translate([0,h,0])
        cylinder(d=hd, h=bt+4);
    translate([w,h,0])
        cylinder(d=hd, h=bt+4);

    translate([w/5, h-10+(bw/2), 0])
    {
        translate([0,0,bt-1])
            cylinder(d=12, h=1);
        cylinder(d=5.5, h=bt);
    }
    translate([4*w/5, h-10+(bw/2), 0])
    {
        translate([0,0,bt-1])
            cylinder(d=12, h=1);
        cylinder(d=5.5, h=bt);
    }
}

