mockup();

module print()
{
    translate([0,0,-27])
        bracket();

    translate([2,0,-27])
        clamp();

    translate([10,10,-21])
        flag();
}

module mockup()
{
    do_mockup();
    bracket();
    clamp();
    flag();
}

module do_mockup()
{
    // LM8LUU
    translate([0,0,-24])
        %cylinder(d=15, h=48, $fn=45);

    // Z-rod 
    translate([0,0,-25])
        %cylinder(d=8, h=100, $fn=30);

    // carriage mount...put an M3 screw in the tab, possibly with an opaque flag on top
    translate([-77.5,-11.2,50])
    mirror([0,1,0])
    rotate([0,-90,180])
        %import("../../z_carriage/original/HC300_Z_Carriage_Left_Make_2.stl");

    // M3 screw
    translate([-32.5,-13.25,13])
        %cylinder(d=3, h=12, $fn=20);

    // endstop sensor
    translate([-44.75,-21.5,35])
        %import("Optical endstop.stl");
}

module flag()
{
    translate(([-32.5,-13.25,21]))
    {
        difference()
        {
            cylinder(d=7, h=5, $fn=30);
            cylinder(d=3, h=4, $fn=20);
        }
        translate([-.75,-3,5])
            cube([1.5,6,7]);
    }
}

module bracket()
{
    difference()
    {
        rotate([0,0,-130])
        translate([-12,-7,27])
            cube([36.15,7,10]);

        // rod cutout
        translate([0,0,-25])
            cylinder(d=8, h=100, $fn=30);

        // screw holes
        rotate([0,0,-130])
        translate([8,10,32])
        rotate([90,0,0])
            cylinder(d=3.5, h=20, $fn=20);

        rotate([0,0,-130])
        translate([-8,10,32])
        rotate([90,0,0])
            cylinder(d=3.5, h=20, $fn=20);

        rotate([0,0,-130])
        translate([-8,-4,32])
        rotate([90,0,0])
            cylinder(d=7, h=3.1, $fn=20);

        rotate([0,0,-130])
        translate([8,-4,32])
        rotate([90,0,0])
            cylinder(d=7, h=3.1, $fn=20);
    }

    // sensor attaches to this part
    difference()
    {
        translate([-26,-18.5,27])
            cube([10.45,7,10]);

        translate([-23,-8,32])
        rotate([90,0,0])
            cylinder(d=2.9, h=20, $fn=20);
    }
}

module clamp()
{
    difference()
    {
        rotate([0,0,-130])
        translate([-12,0,27])
            cube([24,7,10]);

        // rod cutout
        translate([0,0,-25])
            cylinder(d=8, h=100, $fn=30);

        // screw holes
        rotate([0,0,-130])
        translate([8,10,32])
        rotate([90,0,0])
            cylinder(d=3.5, h=20, $fn=20);

        rotate([0,0,-130])
        translate([-8,10,32])
        rotate([90,0,0])
            cylinder(d=3.5, h=20, $fn=20);

        rotate([0,0,-130])
        translate([-8,7.1,32])
        rotate([90,0,0])
            cylinder(d=6.5, h=3.1, $fn=6);

        rotate([0,0,-130])
        translate([8,7.1,32])
        rotate([90,0,0])
            cylinder(d=6.5, h=3.1, $fn=6);
    }
}
