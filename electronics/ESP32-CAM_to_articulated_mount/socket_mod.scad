$fn=30;

translate([0,0,2])
%import("../ESP32-CAM_housing/Bottom.stl");

difference()
{
    import("socket.stl");

    translate([-17,-3,-3])
        cube([8,6,3]);

    translate([-12.6,8,-8])
        cylinder(d=2.9, h=30);
    translate([-12.6,-8,-8])
        cylinder(d=2.9, h=30);
}

