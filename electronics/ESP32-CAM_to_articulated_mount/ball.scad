$fn=90;

translate([0,0,10.3])
{
    translate([10.2,-3.2,-18.075])
    rotate([0,-90,0])
        import("../articulated_camera_mount/mfLink_90.STL");

    translate([-3,-11,-.5])
    rotate([0,0,30])
        import("../articulated_camera_mount/BallNut.STL");
}

difference()
{
    cylinder(d=19, h=8);
    cylinder(d=15, h=8);
}

translate([0,0,8])
    cylinder(d=14, h=5.3, $fn=6);