difference()
{
    union()
    {
        linear_extrude(2, convexity=10)
            polygon([[0,0],[70,0],[50,20],[0,20]]);

        translate([0,18,-5])
            cube([52,2,5]);

        translate([70,0,-18])
        rotate([0,0,45])
        difference()
        {
            cube([2,29,20]);
            translate([0,10,10])
            rotate([0,90,0])
                cylinder(d=5.5, h=2, $fn=30);
        }
    }
    translate([0,20,-20])
        cube([70,10,22]);
}