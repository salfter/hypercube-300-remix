rotate([-90,0,0])
{
    translate([23,0,28])
    rotate([0,0,180])
        %import("Raspberrypimodel.stl");

    difference()
    {
        translate([0,-15,-3])
            cube([66,20,3]);

        translate([18,-5,-4])
            cylinder(d=5.8, $fn=45, h=5);

        translate([48,-5,-4])
            cylinder(d=5.8, $fn=45, h=5);
    }

    difference()
    {
        union()
        {
        translate([0,2,0])
            cube([66,3,12]);

        translate([0,2,51])
            cube([66,3,8]);

        translate([0,2,0])
            cube([8,3,59]);

        translate([58,2,0])
            cube([8,3,59]);

        translate([4,3,7])
        rotate([90,0,0])
            cylinder(d=7, h=3, $fn=30);

        translate([4,3,56])
        rotate([90,0,0])
            cylinder(d=7, h=3, $fn=30);

        translate([62,3,7])
        rotate([90,0,0])
            cylinder(d=7, h=3, $fn=30);

        translate([62,3,56])
        rotate([90,0,0])
            cylinder(d=7, h=3, $fn=30);
        }

        translate([4,8,7])
        rotate([90,0,0])
            cylinder(d=2.3, h=10, $fn=30);

        translate([4,8,56])
        rotate([90,0,0])
            cylinder(d=2.3, h=10, $fn=30);

        translate([62,8,7])
        rotate([90,0,0])
            cylinder(d=2.3, h=10, $fn=30);

        translate([62,8,56])
        rotate([90,0,0])
            cylinder(d=2.3, h=10, $fn=30);
    }

    translate([77,5,30])
    rotate([90,90,0])
        import("buck-converter-v1.stl");

    translate([66,3,4.3])
        cube([3,2,50.5]);
}