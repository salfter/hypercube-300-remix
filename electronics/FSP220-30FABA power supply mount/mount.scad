// settings below are for an FSP220-30FABA 220W supply, but can be 
// adjusted for pretty much any power supply

pw=91; // power supply width
ph=53; // power supply height

w=15;  // bracket width
t=5;   // bracket thickness

g=1.5; // tightening block gap

difference()
{
    union()
    {
        // outer perimeter
        cube([pw+2*t, ph+2*t, w]);

        // tightening block
        translate([pw/2+t, ph+2*t, 0])
        linear_extrude(w, convexity=10)
            polygon([[-15, 0], [15, 0], [5, 10], [-5, 10]]);

        // mounting tabs for 2020
        translate([-20, 0, 0])
            cube([20, t, w]);
        translate([0, -20, 0])
            cube([t, 20, w]);
    }

    // inner perimeter
    translate([t, t, 0])
        cube([pw, ph, w]);

    // holes in tightening block
    translate([0, ph+2*t+5, w/2])
    rotate([0,90,0])
    {
        cylinder(d=3.4, $fn=30, h=pw+2*t);
        cylinder(d=7, $fn=30, h=pw/2+t-2);
        translate([0, 0, pw/2+t+2])
            cylinder(d=6.8, $fn=6, h=pw/2+t);
    }

    // tightening block gap
    translate([pw/2+t-g/2, ph+t, 0])
        cube([g, t+10, w]);

    // holes in mounting tabs
    translate([-10, 0, w/2])
    rotate([-90,0,0])
        cylinder(d=5.5, $fn=30, h=t);
    translate([-10, 3, w/2])
    rotate([-90,0,0])
        cylinder(d=10, $fn=30, h=t);
    translate([0, -10, w/2])
    rotate([0,90,0])
        cylinder(d=5.5, $fn=30, h=t);
    translate([3, -10, w/2])
    rotate([0,90,0])
        cylinder(d=10, $fn=30, h=t);
}


