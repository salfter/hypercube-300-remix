                   .:                     :,                                          
,:::::::: ::`      :::                   :::                                          
,:::::::: ::`      :::                   :::                                          
.,,:::,,, ::`.:,   ... .. .:,     .:. ..`... ..`   ..   .:,    .. ::  .::,     .:,`   
   ,::    :::::::  ::, :::::::  `:::::::.,:: :::  ::: .::::::  ::::: ::::::  .::::::  
   ,::    :::::::: ::, :::::::: ::::::::.,:: :::  ::: :::,:::, ::::: ::::::, :::::::: 
   ,::    :::  ::: ::, :::  :::`::.  :::.,::  ::,`::`:::   ::: :::  `::,`   :::   ::: 
   ,::    ::.  ::: ::, ::`  :::.::    ::.,::  :::::: ::::::::: ::`   :::::: ::::::::: 
   ,::    ::.  ::: ::, ::`  :::.::    ::.,::  .::::: ::::::::: ::`    ::::::::::::::: 
   ,::    ::.  ::: ::, ::`  ::: ::: `:::.,::   ::::  :::`  ,,, ::`  .::  :::.::.  ,,, 
   ,::    ::.  ::: ::, ::`  ::: ::::::::.,::   ::::   :::::::` ::`   ::::::: :::::::. 
   ,::    ::.  ::: ::, ::`  :::  :::::::`,::    ::.    :::::`  ::`   ::::::   :::::.  
                                ::,  ,::                               ``             
                                ::::::::                                              
                                 ::::::                                               
                                  `,,`


https://www.thingiverse.com/thing:2119103
Fused AC Switch Enclosure by Kanata is licensed under the Creative Commons - Attribution - Non-Commercial - Share Alike license.
http://creativecommons.org/licenses/by-nc-sa/3.0/

# Summary

I designed a completely enclosed housing for a fused switch IEC320.
I have it mounted to 2020 aluminum extrusion.
The back cover is a snap-fit and does not require any fasteners.

If you have any suggestions, You can message me.

Non-Commercial:
You do not have permission to print and sell this, either as a whole or as individual components. You do have permission to print and test this item for your own use.

# Parts

2x 4mm  10mm screws.
2x 4mm nuts.
2x 5mm  10mm Socket head Cap Screw
2x 5mm nuts  (file the corners so they fit the 2020 extrusion)

1 Fused Switch
http://www.ebay.ca/itm/10A-250V-Inlet-Module-Plug-Fuse-Switch-Male-Power-Socket-3-Pin-IEC320-C14-/232034779543?hash=item360659c197:g:m4oAAOSw3xJXnvlc