$fn=30;
new_diam=2.6;

translate([-40,-28,0])
{
    translate([199,-597,14.565])
    rotate([180,0,0])
    import("../originals/lcd-housing/LCD_RP3_board_to_Pi_mount_Make_1.stl");

    difference()
    {
        cylinder(d=4, h=3);
        cylinder(d=new_diam, h=3);
    }

    translate([58,0,0])
    difference()
    {
        cylinder(d=4, h=3);
        cylinder(d=new_diam, h=3);
    }

    translate([0,49,0])
    difference()
    {
        cylinder(d=4, h=3);
        cylinder(d=new_diam, h=3);
    }

    translate([58,49,0])
    difference()
    {
        cylinder(d=4, h=3);
        cylinder(d=new_diam, h=3);
    }
}
