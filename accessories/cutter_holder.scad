difference()
{
    cube([20,30,4]);
    translate([10,20,0])
        cylinder(d=5.4, $fn=30, h=4);
}

linear_extrude(10, convexity=10)
    polygon([[7,0],[10,5],[13,0]]);

translate([0,0,10])
linear_extrude(2, convexity=10)
    polygon([[4,0],[10,8],[16,0]]);
