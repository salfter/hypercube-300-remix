$fn=45;

// frame mount...print in ABS
module frame_mount()
{
    difference()
    {
        // main body shape
        rotate([90,0,0])
        linear_extrude(20, convexity=10)
            polygon([[0,0],[0,20],[20,0]]);

        // hole for M5 screw
        translate([0,-10,10])
        rotate([0,90,0])
            cylinder(d=5.4, h=20);

        // countersink for M5 screw
        translate([2,-10,10])
        rotate([0,90,0])
            cylinder(d=11, h=20);

        // hole for M4 threaded insert (https://www.prusa3d.com/content/wysiwyg/fotky/cnc-k/heatset_inserts_all_drawing.png)
        translate([10,-10,0])
            cylinder(d=5.6, h=20);
    }
}

// foot for screw end...print in ABS
module screw_foot()
{
    difference()
    {
        cylinder(d1=10, d2=8, h=5);
        translate([0,0,2])
            cylinder(d=4.8, h=10);
    }
}

frame_mount();

translate([10,10,0])
    screw_foot();
