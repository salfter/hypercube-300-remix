$fn=45;
tol=0.25;

use <leveler/leveler.scad>

//model(); // view frame within bed assembly
production(); // set up frame parts for printing

module production()
{
    translate([10,0,0])
    rotate([0,-90,0])
        front_side();

    translate([30,0,0])
    rotate([0,90,0])
    mirror([1,0,0])
        front_side();

    translate([50,4,100])
    rotate([-90,0,0])
        rear();

    translate([50,30,4])
        front_t_bot();

    translate([70,60,5])
    rotate([0,180,0])
        front_t_top();

    translate([90,5,0])
    rotate([90,0,0])
        center_top();

    translate([110,60,0])
    rotate([0,180,-90])
        center_bot();

    translate([110,85,0])
    rotate([0,180,-90])
        center_bot();
}
    
module extrusion(h)
{
    color("gray")
    translate([-25,30,0])
    linear_extrude(h, convexity=10)
        import("20mm-tslot/20mm-M5-W1pt6-X1.dxf");
}

module z_carriage()
{
    // extrusions

    translate([0,0,20])
    {
        translate([25,20,0])
        rotate([-90,0,0])
            %extrusion(235);
        translate([365,20,0])
        rotate([-90,0,0])
            %extrusion(235);
        rotate([0,90,0])
            %extrusion(410);
        translate([0,255,0])
        rotate([0,90,0])
            %extrusion(410);
    }

    // brackets

    translate([35,10,10])
    rotate([90,0,90])
    color("red")
        %import("../frame_braces/hc300_z_carriage_t_bracket.stl");

    translate([375,10,10])
    rotate([90,0,90])
    color("red")
        %import("../frame_braces/hc300_z_carriage_t_bracket.stl");

    translate([35,265,10])
    rotate([90,0,-90])
    color("red")
        %import("../frame_braces/hc300_z_carriage_t_bracket.stl");

    translate([375,265,10])
    rotate([90,0,-90])
    color("red")
        %import("../frame_braces/hc300_z_carriage_t_bracket.stl");

    translate([90,-20,50])
    rotate([0,-90,0])
    color("red")
        %import("../z_carriage/original/HC300_Z_Carriage_Left_Make_2.stl");

    translate([320,-20,50])
    mirror([1,0,0])
    rotate([0,-90,0])
    color("red")
        %import("../z_carriage/original/HC300_Z_Carriage_Left_Make_2.stl");

    translate([320,295,50])
    rotate([0,-90,180])
    color("red")
        %import("../z_carriage/original/HC300_Z_Carriage_Left_Make_2.stl");

    translate([90,295,50])
    mirror([1,0,0])
    rotate([0,-90,180])
    color("red")
        %import("../z_carriage/original/HC300_Z_Carriage_Left_Make_2.stl");

    // levelers

    color("red")
    translate([70,0,20])
    rotate([0,180,90])
        %frame_mount();

    color("red")
    translate([320,0,20])
    rotate([0,180,90])
        %frame_mount();

    color("red")
    translate([215,275,20])
    rotate([0,180,-90])
        %frame_mount();
}

module screw_locations()
{
    // screw locations

    translate([80,-10,15])
    {
        %cylinder(d=1, h=10);
        translate([0,0,12])
        rotate([180,0,0])
        color("red")
            %screw_foot();
    }

    translate([330,-10,15])
    {
        %cylinder(d=1, h=10);
        translate([0,0,12])
        rotate([180,0,0])
        color("red")
            %screw_foot();
    }

    translate([205,285,15])
    {
        %cylinder(d=1, h=10);
        translate([0,0,12])
        rotate([180,0,0])
        color("red")
            %screw_foot();
    }
}

module bed()
{
    translate([55,-12.5,30])
    color("silver")
        %cube([300,300,6.35]);
}

module model()
{
    z_carriage();
    screw_locations();
    bed();
    bed_frame();
}

module bed_frame()
{
    translate([70,-20,25])
        front_side();
    translate([340,-20,25])
    mirror([1,0,0])
        front_side();
    translate([170,-20,25])
    {
        front_t_bot();
        front_t_top();
    }

    translate([195,195,25])
        rear();
    translate([195,80,25])
    {
        center_top();
        translate([4,-25-2*tol,0])
            center_bot();
        translate([4,80,0])
            center_bot();
    }
}

module front_side()
{
    difference()
    {
        cube([100,20,5]);
        translate([10,10,0])
            cylinder(d=10.5, h=2);
    }
    translate([0,0,-4])
        cube([100,4,9]);
    translate([0,16,-4])
        cube([100,4,9]);
    translate([0,0,2])
    cube([20,6,8]);
}

module front_t_bot()
{
    translate([-30,4+tol,-4])
        cube([130,12-2*tol,4-tol]);
    translate([35-6+tol,4+tol,-4])
        cube([12-2*tol,70,4-tol]);
}

module front_t_top()
{
    translate([tol,0,0])
        cube([70-2*tol,20,5]);
    translate([tol,0,-4])
        cube([70-2*tol,4,4]);
    translate([tol,16,-4])
        cube([29-tol,4,4]);
    translate([41,16,-4])
        cube([29-tol,4,4]);
    translate([25,0,0])
        cube([20,100,5]);
    translate([25,16,-4])
        cube([4,84,4]);
    translate([41,16,-4])
        cube([4,84,4]);
}

module rear()
{
    difference()
    {
        cube([20,100,5]);
        translate([10,90,0])
            cylinder(d=10.5, h=2);
    }
    translate([0,0,-4])
        cube([4,100,4]);
    translate([16,0,-4])
        cube([4,100,4]);
    translate([0,94,2])
        cube([20,6,8]);
}

module center_top()
{
    translate([0,tol,0])
    {
        cube([20,115-2*tol,5]);
        translate([0,0,-4])
            cube([4,115-2*tol,4]);
        translate([16,0,-4])
            cube([4,115-2*tol,4]);
    }
}

module center_bot()
{
    translate([tol,0,-4])
        cube([12-2*tol,70-2*tol,4-tol]);
}